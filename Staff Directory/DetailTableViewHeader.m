//
//  DetailTableViewHeader.m
//  Staff Directory
//
//  Created by user on 1/18/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import "DetailTableViewHeader.h"


@implementation DetailTableViewHeader

@synthesize nameField, titleField, sectionField, photoButton, delegate;





- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		
		UIColor *background = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"fabric-texture.png"]];
		self.backgroundColor = [UIColor clearColor];
		self.backgroundColor = background;

		
		
		
        self.nameField = [[UITextField alloc]initWithFrame:CGRectMake(frame.origin.x+123, frame.origin.y+20, 194, 30)];
		self.nameField.font = [UIFont fontWithName:@"Helvetica" size:19];
		self.nameField.textColor = [UIColor blackColor];
		self.nameField.placeholder = @"First Last";
		self.nameField.backgroundColor = [UIColor whiteColor];
		self.nameField.tag = 0;

		self.nameField.returnKeyType = UIReturnKeyNext;
		self.nameField.autocapitalizationType = UITextAutocapitalizationTypeWords;
		[self addSubview:self.nameField];
		

		self.titleField = [[UITextField alloc]initWithFrame:CGRectMake(frame.origin.x+123, frame.origin.y+96, 194, 30)];
		self.titleField.font = [UIFont fontWithName:@"Helvetica" size:11];
		self.titleField.textColor = [UIColor blackColor];
		self.titleField.placeholder = @"Title";
		self.titleField.backgroundColor = [UIColor whiteColor];
		self.titleField.tag = 1;

		self.titleField.returnKeyType = UIReturnKeyNext;
		self.titleField.autocapitalizationType = UITextAutocapitalizationTypeWords;
		[self addSubview:self.titleField];
		
		
		self.sectionField = [[UITextField alloc]initWithFrame:CGRectMake(frame.origin.x+123, frame.origin.y+58, 194, 30)];
		self.sectionField.font = [UIFont fontWithName:@"Helvetica" size:15];
		self.sectionField.placeholder = @"Section";
		self.sectionField.textColor = [UIColor blackColor];
		self.sectionField.backgroundColor = [UIColor whiteColor];
		self.sectionField.tag = 2;

		self.sectionField.returnKeyType = UIReturnKeyNext;
		self.sectionField.autocapitalizationType = UITextAutocapitalizationTypeWords;
		[self addSubview:sectionField];
		
		
		self.photoButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.origin.x+20, frame.origin.y+20, 75, 106)];
		self.photoButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:11];
		[self.photoButton setTitle:@"Add Photo" forState:UIControlStateNormal];
		self.photoButton.titleLabel.textColor = [UIColor blackColor];
		self.photoButton.backgroundColor = [UIColor grayColor];
		[self.photoButton setClipsToBounds:YES];
		
		[self addSubview:self.photoButton];
    }
    return self;
}


-(void) awakeFromNib{
	[super awakeFromNib];
	
	//[self addSubview:self.containerView];
}


-(void)setDelegate:(id)newDelegate{
	nameField.delegate = newDelegate;//this doesn't seem to be setable here.
	self.titleField.delegate = newDelegate;
	self.sectionField.delegate = newDelegate;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
