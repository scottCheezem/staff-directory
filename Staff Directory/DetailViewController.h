//
//  DetailViewController.h
//  Staff Directory
//
//  Created by user on 1/15/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "MasterViewController.h"
#import "StaffPerson.h"
#import "EditTableViewCell.h"
#import "DetailTableViewHeader.h"
//#import "StaffPhotoViewController.h"

@interface DetailViewController : UIViewController
<UISplitViewControllerDelegate,
UITableViewDelegate,
UITableViewDataSource,
MFMailComposeViewControllerDelegate,
MFMessageComposeViewControllerDelegate,
UITextFieldDelegate,
UIImagePickerControllerDelegate,
UIPopoverControllerDelegate,
ABUnknownPersonViewControllerDelegate,
UIActionSheetDelegate,
UINavigationControllerDelegate>
{
	NSMutableDictionary *contactDictionary;
	DetailTableViewHeader *detailTableHeaderView;
	UIButton *exportButton;
	UIActionSheet *comunicateSheet;
	UIActionSheet *cameraOrPhotoSheet;
	UIImagePickerController *imagePicker;
	
}

@property(strong, nonatomic)UIPopoverController *popOverController;

@property(strong, nonatomic)NSManagedObjectContext *saveObjectContext;

@property(strong, nonatomic)StaffPerson *thisPerson;

@property (strong, nonatomic) IBOutlet UITableView *contactTable;

-(void)resetView;


@end
