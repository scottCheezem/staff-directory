//
//  MasterViewController.h
//  Staff Directory
//
//  Created by user on 1/15/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

#import "NSMutableURLRequest+sendPost.h"

#import <CoreData/CoreData.h>
#import "StaffPerson.h"

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate,
	UISearchBarDelegate,
UISearchDisplayDelegate>{
	NSMutableData *requestData;
	NSString *searchString;

}


@property BOOL searching;

@property (strong, nonatomic) DetailViewController *detailViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property(strong, nonatomic) UISearchBar *searchBar;

@end
