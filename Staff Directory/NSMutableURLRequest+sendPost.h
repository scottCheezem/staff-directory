//
//  NSMutableURLRequest+sendPost.h
//  doorControl
//
//  Created by user on 10/2/12.
//  Copyright (c) 2012 Scott Cheezem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+Additions.h"



@interface NSMutableURLRequest (sendPost)
-(void)sendPost:(NSString*)urlString postString:(NSString*)postString delegate:(id)delegate;
-(void)sendPostBasicAuth:(NSString*)urlString postString:(NSString*)postString userName:(NSString*)userName passWord:(NSString*)passWord delegate:(id)delegate;

@end
