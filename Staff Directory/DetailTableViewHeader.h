//
//  DetailTableViewHeader.h
//  Staff Directory
//
//  Created by user on 1/18/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTableViewHeader : UIView{
	/*UITextField *nameField;
	UITextField *titleField;
	UITextField *sectionField;
	UIButton *photoButton;*/

}


@property(nonatomic,retain) id delegate;


@property (nonatomic, retain)  UITextField *nameField;
@property (nonatomic, retain)  UITextField *titleField;
@property (nonatomic, retain)  UITextField *sectionField;
@property (nonatomic, retain)  UIButton *photoButton;
@end
