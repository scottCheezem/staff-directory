//
//  StaffPerson.h
//  Staff Directory
//
//  Created by user on 1/15/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StaffPerson : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSString * serviceArea;
@property (nonatomic, retain) NSString * jobTitle;
@property (nonatomic, retain) NSString * email;
-(NSString*)lastInitial;
@end
