//
//  DetailViewController.m
//  Staff Directory
//
//  Created by user on 1/15/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//


//Still need to be able to edit!
//dont forget about selecting/displaying photo!



#import "DetailViewController.h"

@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController
@synthesize  contactTable;
@synthesize saveObjectContext;

#pragma mark - Managing the detail item

- (void)setThisPerson:(StaffPerson *)newPerson
{
    if (_thisPerson != newPerson) {
        _thisPerson = newPerson;
        
        // Update the view.
        //[self configureView];
		[self populateStaffPersonData];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

-(void)resetView{
	
	
	self.thisPerson.firstName = nil;
	self.thisPerson.lastName = nil;
	self.thisPerson.serviceArea = nil;
	self.thisPerson.jobTitle = nil;
	self.thisPerson.photo = nil;
	
	self.thisPerson.email = @"";
	self.thisPerson.phone = @"";
	[self populateStaffPersonData];
	
}


-(void)populateStaffPersonData{

	
	

	
	//since "nil nil" is stil " "
	//if (![self.thisPerson.firstName isEqual: @""] && ![self.thisPerson.lastName isEqual: @""]) {
	if(self.thisPerson.firstName != nil && self.thisPerson.lastName != nil){
		detailTableHeaderView.nameField.text = [NSString stringWithFormat:@"%@ %@", self.thisPerson.firstName, self.thisPerson.lastName];
	}else{
		detailTableHeaderView.nameField.text = nil;
	}

	
	detailTableHeaderView.titleField.text = self.thisPerson.jobTitle;
	
	detailTableHeaderView.sectionField.text = self.thisPerson.serviceArea;
	
	
	///WWWWTTTTTFFFFF I reallydon't know why some of these aren't populated.  But ugly code is better than broken code

	if(self.thisPerson.phone == nil || self.thisPerson.email == nil){
		contactDictionary = [[NSMutableDictionary alloc]initWithObjects:[NSArray arrayWithObjects:@"", @"",  nil] forKeys:[NSArray arrayWithObjects:@"Phone", @"Email",  nil]];
	}else{
	
	contactDictionary = [[NSMutableDictionary alloc]initWithObjects:[NSArray arrayWithObjects:self.thisPerson.phone, self.thisPerson.email,  nil] forKeys:[NSArray arrayWithObjects:@"Phone", @"Email",  nil]];
	}
	
	
	
	if(self.thisPerson.photo != nil){
		//setup image
		CGRect bounds;
		bounds.origin = CGPointZero;
		UIImage *staffPhotoImage = [UIImage imageWithData:self.thisPerson.photo];
		bounds.size	 = staffPhotoImage.size;
		detailTableHeaderView.photoButton.bounds = bounds;
		[detailTableHeaderView.photoButton setImage:staffPhotoImage forState:UIControlStateNormal];
		[detailTableHeaderView.photoButton setTitle:nil forState:UIControlStateNormal];
	}else{
		
		[detailTableHeaderView.photoButton setTitle:@"Add Photo" forState:UIControlStateNormal];
		detailTableHeaderView.photoButton.titleLabel.textColor = [UIColor blackColor];
		[detailTableHeaderView.photoButton setImage:nil forState:UIControlStateNormal];
		detailTableHeaderView.photoButton.backgroundColor = [UIColor grayColor];
		[detailTableHeaderView.photoButton setClipsToBounds:YES];
	}
	
	
	[self.contactTable reloadData];
	
}




-(void)setHeaderTextEditEnabled:(BOOL)enabled{
	if (enabled) {
		detailTableHeaderView.userInteractionEnabled = YES;
		
		detailTableHeaderView.nameField.enabled = YES;
		detailTableHeaderView.nameField.backgroundColor = [UIColor whiteColor];
		detailTableHeaderView.nameField.borderStyle = UITextBorderStyleRoundedRect;
		
		
		detailTableHeaderView.titleField.enabled = YES;
		detailTableHeaderView.titleField.backgroundColor = [UIColor whiteColor];
		detailTableHeaderView.titleField.borderStyle = UITextBorderStyleRoundedRect;
		
		detailTableHeaderView.sectionField.enabled = YES;
		detailTableHeaderView.sectionField.backgroundColor = [UIColor whiteColor];
		detailTableHeaderView.sectionField.borderStyle = UITextBorderStyleRoundedRect;


		//detailTableHeaderView.photoButton.enabled = YES;
		//detailTableHeaderView.userInteractionEnabled = YES;

		
	}else{
		detailTableHeaderView.nameField.borderStyle = UITextBorderStyleNone;
		detailTableHeaderView.nameField.backgroundColor = [UIColor clearColor];
		detailTableHeaderView.nameField.enabled = NO;
		
		detailTableHeaderView.titleField.borderStyle = UITextBorderStyleNone;
		detailTableHeaderView.titleField.backgroundColor = [UIColor clearColor];
		detailTableHeaderView.titleField.enabled = NO;
		
		detailTableHeaderView.sectionField.borderStyle = UITextBorderStyleNone;
		detailTableHeaderView.sectionField.backgroundColor = [UIColor clearColor];
		detailTableHeaderView.sectionField.enabled = NO;
		

		//detailTableHeaderView.photoButton.enabled = YES;
		//detailTableHeaderView.userInteractionEnabled = NO;
		
		

	}
	
	
	
}



 - (void)configureView
 {
		 
		 UIView *tableFooter = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.contactTable.frame.size.width, 50)];
		 
		 exportButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		 
		 
		 [exportButton setTitle:@"Export Contact" forState:UIControlStateNormal];
		 
		 [exportButton addTarget:self action:@selector(exportContact) forControlEvents:UIControlEventTouchDown];
		 
		 
		 [tableFooter addSubview:exportButton];
		 
		 [self.contactTable setTableFooterView:tableFooter];
		 
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Do any additional setup after loading the view, typically from a nib.
	
	self.title = @"Info";
	
	// Create and set the table header view.

	detailTableHeaderView = [[DetailTableViewHeader alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 146)];
	
	[self.contactTable setTableHeaderView:detailTableHeaderView];
	

	[detailTableHeaderView.photoButton addTarget:self action:@selector(photoTapped:) forControlEvents:UIControlEventTouchDown];
	
	detailTableHeaderView.delegate = self;

	self.contactTable.allowsSelectionDuringEditing = YES;

	self.contactTable.delegate = self;
	
#warning this should go away
	[self configureView];
	
	
	[self populateStaffPersonData];
	
	[self setHeaderTextEditEnabled:NO];
	
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
	

}


-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
	
	
}


-(void)viewDidLayoutSubviews{
	
	
	
	[exportButton setTitle:@"Export Contact" forState:UIControlStateNormal];
	
	[exportButton setFrame:CGRectMake(40, 10, self.contactTable.contentSize.width-80, 46)];
	
	[exportButton setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	

	
	
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}


#pragma mark ImagePickerContrller delegate methods





- (void)photoTapped:(id)sender {
	if(self.editing){
		[self showCameraOrPhotoActionSheet];
	}
	
	
	
}


-(void)useCamera{
	if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
		imagePicker = [[UIImagePickerController alloc]init];
		//imagePicker.allowsEditing = YES;//do we want this functionality?
		imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
		imagePicker.delegate = self;
		//[self presentModalViewController:imagePicker animated:YES];
		
		if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
			self.popOverController = [[UIPopoverController alloc]initWithContentViewController:imagePicker];
			self.popOverController.delegate = self;
			[self.popOverController presentPopoverFromRect:self.view.bounds inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
			//[self.popOverController presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionDown animated:NO];
		}else{
			
			[self presentViewController:imagePicker animated:YES completion:nil];
		}
		
		
		
		
	}else{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera Available"
														message:@"A camera is required to take pictures"
													   delegate:self
											  cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		
	}
}


-(void)usePhotoLibrary{
	if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
		imagePicker = [[UIImagePickerController alloc]init];
		imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		//imagePicker.allowsEditing = YES;
		imagePicker.delegate = self;
		
		
		
		if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
			self.popOverController = [[UIPopoverController alloc]initWithContentViewController:imagePicker];
			self.popOverController.delegate = self;
			[self.popOverController presentPopoverFromRect:self.view.bounds inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
			//[self.popOverController presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionDown animated:NO];
		}else{
			
			[self presentViewController:imagePicker animated:YES completion:nil];
		}
		
		
	}else{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Photos Available",@"No Photos Available")
														message:NSLocalizedString(@"An existing photo gallery is not available","@An existing photo gallery is not available")
													   delegate:self
											  cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
		[alert show];
		
	}
}



-(void)showCameraOrPhotoActionSheet{
	cameraOrPhotoSheet = [[UIActionSheet alloc]initWithTitle:@"Select a Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From Library", @"Camera", nil];
	[cameraOrPhotoSheet showInView:self.view.window];
}




-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{

	UIImage *newImage = [info objectForKey:UIImagePickerControllerEditedImage];
	if (!newImage){
		newImage = [info objectForKey:UIImagePickerControllerOriginalImage];
	}
		//Create a new thumbnail image
		
		CGSize size = detailTableHeaderView.photoButton.bounds.size;
		
		CGRect rect = CGRectMake(0.0, 0, size.width, size.height);
		
		UIGraphicsBeginImageContext(rect.size);
		[newImage drawInRect:rect];
		UIImage *thumbNail = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		NSData *newImageData = UIImagePNGRepresentation(thumbNail);
		self.thisPerson.photo = newImageData;
		
		//[self configureDetailTableHeaderView];
	[self populateStaffPersonData];
	
	[picker dismissViewControllerAnimated:YES completion:nil];
	
}

#pragma mark Popover Controller delegate methods

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
	
}

-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
	return YES;
}

#pragma mark tableview delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return contactDictionary.allKeys.count;//headers.count;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
	return 1;
}
-(NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section{
//	NSLog(@"%@", [[contactDictionary allKeys] objectAtIndex:section]);
	return [contactDictionary.allKeys objectAtIndex:section];
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	
	static NSString *cellId = @"personCell";
	//EditTableViewCell *cell = (EditTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellId];
	
	//if(cell==nil){
		EditTableViewCell *cell = [[EditTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
		
	//}
	
	NSString *cellValue = [contactDictionary.allValues objectAtIndex:indexPath.section];
	NSString *placeHolderValue = [contactDictionary.allKeys objectAtIndex:indexPath.section];
	cell.editTextField.text = cellValue;
	cell.editTextField.placeholder = placeHolderValue;
	cell.editTextField.delegate = self;

	//[cell.editTextField sizeToFit];
	cell.editTextField.adjustsFontSizeToFitWidth = YES;

	
	//the email cell is set automatially. so we only want to edit the phoneCell
	if(self.editing && indexPath.section != 0){
		cell.editTextField.enabled = YES;
	}else{
		cell.editTextField.enabled = NO;
	}
	//[cell setEditing:self.editing];
	
	return cell;
	

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	if(!self.editing){
		NSLog(@"%d", indexPath.section);
		NSString *section = [contactDictionary.allKeys objectAtIndex:indexPath.section];
		NSLog(@"%@", section);
		
		
		if([section isEqualToString:@"Email"]){
			[self composeMailMessage];
		}else if([section isEqualToString:@"Phone"]){


			NSString *model = [UIDevice currentDevice].model;
			if([model isEqualToString:@"iPhone"]){
					
				//sms or call...leaving this in here
				//[self showShareSheet:nil];
				//[self compseSMSMessage];
				
				//instead of incorperating views into this app, open the appropirate nateive app. 
				//[[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"sms:%@", self.thisPerson.phone]]];
				//[[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", self.thisPerson.phone]]];
				
				[self showCommunicateSheet];
				
			}
		}
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
	}
}

#pragma mark maildialog delegate

-(void)composeMailMessage{
	if([MFMailComposeViewController canSendMail]){
		MFMailComposeViewController *compseMailView = [[MFMailComposeViewController alloc]init];
		compseMailView.mailComposeDelegate = self;
		[compseMailView setToRecipients:[NSArray arrayWithObjects:self.thisPerson.email, nil]];
		[self presentViewController:compseMailView animated:YES completion:nil];
	}else{
		NSLog(@"bummer");
	}
}


-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark sms delegate

-(void)compseSMSMessage{
	if([MFMessageComposeViewController canSendText]){
		MFMessageComposeViewController *composeSMSView = [[MFMessageComposeViewController alloc]init];
		composeSMSView.messageComposeDelegate = self;
		[composeSMSView setRecipients:[NSArray arrayWithObjects:self.thisPerson.phone, nil]];
		[self presentViewController:composeSMSView animated:YES completion:nil];
	}else{
		
	}
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark Action and Share Sheets
-(void)showCommunicateSheet{
	
	comunicateSheet = [[UIActionSheet alloc]initWithTitle:@"Contact Via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call", @"Send Instant Message", nil];
	[comunicateSheet showInView:self.view.window];
	
	
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	
	
	
	if (actionSheet == comunicateSheet) {
		
	
		switch (buttonIndex) {
			case 0:
				//open phone url
				[[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", self.thisPerson.phone]]];
				break;
				case 1:
				[self compseSMSMessage];
			default:
				break;
		}
	}else if(actionSheet == cameraOrPhotoSheet){
		switch (buttonIndex) {
			case 0:
				//photo
				[self usePhotoLibrary];
				break;
			case 1:
				//camera
				[self useCamera];
				break;
			default:
				break;
		}
	}
	
	
}



//wait.  saec this as an option to add to both the click callback for phone and email cells!!! thats what its really there for!!
-(void)showShareSheet:(id)sender {
//  resitric activities to phone, and or sms
	
    NSURL *smsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"sms:%@", self.thisPerson.phone]];
    NSURL *telUrl = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", self.thisPerson.phone]];
	
//	UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypeMessage
	
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[smsUrl,telUrl] applicationActivities:nil];
//    [self.navigationController presentModalViewController:activityViewController animated:YES];
	[self.navigationController presentViewController:activityViewController animated:YES completion:nil];

}

#pragma mark view editing



-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
	
	
	[super setEditing:editing animated:animated];
	
	[self.navigationItem setHidesBackButton:editing animated:YES];
	
	[self.contactTable reloadData];
	[self setHeaderTextEditEnabled:editing];
		
}


#pragma mark - textfield delgate methods

-(void)textFieldDidBeginEditing:(UITextField *)textField{
	//scroll to active textfield?
	
	NSLog(@"Keyborad in");
	if([textField.superview.superview isKindOfClass:[EditTableViewCell class]]){
		UITableViewCell *aCell = (EditTableViewCell*)textField.superview.superview;
		CGPoint scrollTo = [aCell convertPoint:aCell.bounds.origin toView:self.contactTable];
		[self.contactTable setContentOffset:scrollTo animated:YES];
	}
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
	//dismiss keyboard?
	NSLog(@"Dissmissing Keyboard");
	[self.contactTable scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewRowAnimationTop animated:YES];
	//write new data to staffPerson
	
	[self updateContactData];
	
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
	return YES;
}


-(void)updateContactData{
	
	//how to get the first and last name
	
	NSArray *firstLast = [detailTableHeaderView.nameField.text componentsSeparatedByString:@" "];
	self.thisPerson.firstName = [[firstLast objectAtIndex:0]capitalizedString];
	self.thisPerson.lastName = [[firstLast objectAtIndex:1]capitalizedString];
	
						 
	self.thisPerson.email = [NSString stringWithFormat:@"%@.%@@osumc.edu", [self.thisPerson.firstName lowercaseString], [self.thisPerson.lastName lowercaseString]];
	[contactDictionary setObject:self.thisPerson.email forKey:@"Email"];
						 
	self.thisPerson.serviceArea = [detailTableHeaderView.sectionField.text capitalizedString];
	self.thisPerson.jobTitle = [detailTableHeaderView.titleField.text capitalizedString];
	
	//update the email - generate based on first.last@osumc.edu
	EditTableViewCell *emailCell = (EditTableViewCell*)[self.contactTable cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
	if(emailCell){
		
		
		//this could be left in, if we wanted to be able to set the email field to something other than the default.
		/*self.thisPerson.email = emailCell.editTextField.text;
		NSLog(@"%@", self.thisPerson.email);
		[contactDictionary setObject:self.thisPerson.email forKey:@"Email"];*/
		
		
		
	}
	//update the phone Number
	EditTableViewCell *phoneCell = (EditTableViewCell*)[self.contactTable cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]];
	if(phoneCell){
		self.thisPerson.phone = phoneCell.editTextField.text;
		[contactDictionary setObject:self.thisPerson.phone forKey:@"Phone"];
	
	}

		
	NSError *error = nil;
	
	
	
	if(![self.saveObjectContext save:&error]){
		NSLog(@"%@", error.localizedDescription);
		abort();
	}
	[contactTable reloadData];
}


-(void)exportContact{
	
	ABUnknownPersonViewController *unknownController = [[ABUnknownPersonViewController alloc]init];
	unknownController.allowsAddingToAddressBook = YES;
	unknownController.allowsActions = NO;
	unknownController.unknownPersonViewDelegate = self;
	
	
	
	
	ABRecordRef aContact = ABPersonCreate();
	CFErrorRef anError = NULL;
	
	ABPersonSetImageData(aContact, (__bridge CFDataRef)(self.thisPerson.photo), nil);
	
	
	
	ABMultiValueRef email = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    bool didAddEmail = ABMultiValueAddValueAndLabel(email, (__bridge CFStringRef)self.thisPerson.email, kABWorkLabel, NULL);
    
	
	
	NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.thisPerson.firstName, self.thisPerson.lastName];
	
	ABRecordSetValue(aContact, kABPersonFirstNameProperty, (__bridge CFTypeRef)(self.thisPerson.firstName), NULL);
    ABRecordSetValue(aContact, kABPersonLastNameProperty, (__bridge CFStringRef)(self.thisPerson.lastName), NULL);
    
    if (self.thisPerson.jobTitle != nil) {
        ABRecordSetValue(aContact, kABPersonJobTitleProperty, (__bridge CFStringRef)self.thisPerson.jobTitle, NULL);
    }
    
    if (self.thisPerson.serviceArea!= nil) {
        ABRecordSetValue(aContact, kABPersonDepartmentProperty, (__bridge CFStringRef)[NSString stringWithFormat:@"%@ - Health Sciences Library",self.thisPerson.serviceArea], NULL);
    }
    
    ABRecordSetValue(aContact, kABPersonOrganizationProperty, @"The Ohio State University", NULL);
    
	if (didAddEmail)
	{
		ABRecordSetValue(aContact, kABPersonEmailProperty, email, &anError);
        if (self.thisPerson.phone != nil) {
            ABMultiValueRef phone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            ABMultiValueAddValueAndLabel(phone, (__bridge CFStringRef)self.thisPerson.phone, kABWorkLabel, NULL);
            ABRecordSetValue(aContact, kABPersonPhoneProperty, phone, &anError);
            CFRelease(phone);
        }
        
		if (anError == NULL)
		{
			unknownController.displayedPerson = aContact;
			unknownController.alternateName = fullName;
			unknownController.title = @"Contact";//NSLocalizedString(@"Contact", @"Contact");
		}
		else
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
															message:@"Could not create unknown user"
														   delegate:nil
												  cancelButtonTitle:@"Cancel"
												  otherButtonTitles:nil];
			[alert show];
		}
	}
	CFRelease(email);
	CFRelease(aContact);
    
    [self.navigationController pushViewController:unknownController animated:YES];

	
	
	
}
#pragma mark - ABUnknownPersonViewControllerDelegate delegate methods
- (void)unknownPersonViewController:(ABUnknownPersonViewController *)unknownPersonView didResolveToPerson:(ABRecordRef)person {
    [self.navigationController popToViewController:self animated:YES];
}



@end
