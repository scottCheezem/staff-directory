//
//  MasterViewController.m
//  Staff Directory
//
//  Created by user on 1/15/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"


#define SEARCH_BAR_HEIGHT 44

@interface MasterViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation MasterViewController
//@synthesize managedObjectContext, fetchedResultsController;

- (void)awakeFromNib
{
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
	    self.clearsSelectionOnViewWillAppear = NO;
	    self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
	}
    [super awakeFromNib];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.title = @"Staff Directory";
	
	requestData = [[NSMutableData alloc]init];
	searchString = @"";
	
	// Do any additional setup after loading the view, typically from a nib.
	self.navigationItem.leftBarButtonItem = self.editButtonItem;

	
	
	UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createNewStaffPerson)];
	self.navigationItem.rightBarButtonItem = addButton;
	
	
	self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject]topViewController];
	self.detailViewController.saveObjectContext = self.managedObjectContext;

	
	//Do we have data already stored?  if not grab it from the feed.
	NSError *err;
	
	[self.managedObjectContext executeFetchRequest:[self.fetchedResultsController fetchRequest] error:&err];
	
	if([[self.fetchedResultsController fetchedObjects]count]==0){
		
		//[self createNewStaffPerson];
		
		
		
		//we seem to be having some weird problems pulling data form the endpoint, so lets just load it from a file
		NSString *urlString = @"https://hsl.osu.edu/staffjson";
		NSMutableURLRequest *staffDirectoryRequest = [[NSMutableURLRequest alloc]init];
		[staffDirectoryRequest sendPost:urlString postString:nil delegate:self];
		
		[self insertNewObject:[self addMyself]];
		
		//we can load json data from a file, but the images are still at the same host, which seems to have problems on occasion :(
		/*
		NSData *jsonDataFromFile = [NSData dataWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"staffjson" ofType:@"json"]];
		[self processJSONData:jsonDataFromFile];
		*/
		 
	}else{
		self.detailViewController.thisPerson = [[self.fetchedResultsController fetchedObjects]objectAtIndex:0];

		//this will break if there is no data in the table...
		//[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
	}
	
	
	
	self.searchBar = [[UISearchBar alloc]init];
	self.searchBar.delegate = self;
	self.searchBar.showsCancelButton = YES;
	[self.searchBar setAutocapitalizationType:UITextAutocapitalizationTypeWords];
	[self.searchBar sizeToFit];
	self.searchBar.showsCancelButton = NO;
	self.searchDisplayController.delegate = self;
	self.tableView.tableHeaderView = self.searchBar;
	
	
	
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)insertNewObject:(id)sender{
	
	NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
//	NSEntityDescription *staffPersonEntity = [[self.fetchedResultsController fetchRequest]entity];
	NSManagedObject *newStaffPerson = [NSEntityDescription insertNewObjectForEntityForName:@"StaffPerson" inManagedObjectContext:context];
	NSDictionary *node = (NSDictionary*)sender;

	NSString *field_firstname = @"";
	NSString *field_lastname = @"";
	NSString *email = @"";
	NSString *field_jobtitle = @"";
	NSString *serviceArea = @"";
	NSString *phone = @"";
	
	NSData *imgData = [[NSData alloc]init];
	
	
	if ([node objectForKey:@"field_firstname"] != nil && [node objectForKey:@"field_lastname"] != nil && [node objectForKey:@"field_jobtitle"] != nil && [node objectForKey:@"Service Area"] != nil && [node objectForKey:@"Phone"] != nil && [node objectForKey:@"field_staffphoto"]!= nil) {
		
	
		field_firstname = [node objectForKey:@"field_firstname"];
		field_firstname = [field_firstname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		field_lastname = [node objectForKey:@"field_lastname"];
		field_lastname = [field_lastname  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		email = [NSString stringWithFormat:@"%@.%@@osumc.edu", [field_firstname lowercaseString], [field_lastname lowercaseString]];
		field_jobtitle = [node objectForKey:@"field_jobtitle"];
		
		serviceArea = [node objectForKey:@"Service Area"];
		serviceArea = [serviceArea stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
		phone = [node objectForKey:@"Phone"];
		phone = [phone stringByReplacingOccurrencesOfString:@"." withString:@"-"];
		NSURL *imgUrl = [NSURL URLWithString:[node objectForKey:@"field_staffphoto"]];
		imgData = [NSData dataWithContentsOfURL:imgUrl];
	
		[newStaffPerson setValue:field_firstname forKey:@"firstName"];
		[newStaffPerson setValue:field_lastname forKey:@"lastName"];
		[newStaffPerson setValue:email forKey:@"email"];
		[newStaffPerson setValue:phone forKey:@"phone"];
		[newStaffPerson setValue:serviceArea forKey:@"serviceArea"];
		[newStaffPerson setValue:field_jobtitle forKey:@"jobTitle"];
		[newStaffPerson setValue:imgData forKey:@"photo"];
		
		// Save the context.
		NSError *error = nil;
		if (![context save:&error]) {
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			//abort();
		}
	}
}




#pragma mark - Table View delgate

-(NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView{
	NSMutableArray *sectionIndexTitles = [[NSMutableArray alloc]initWithObjects:UITableViewIndexSearch, nil];
	
	
	[sectionIndexTitles addObjectsFromArray:self.fetchedResultsController.sectionIndexTitles];
	if(self.searching){
		return nil;
	}else{
		return sectionIndexTitles;
	}
}



- (NSInteger) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    if (index == 0)
    {
		//CGPoint start = CGPointMake(0, 20);
        [tableView setContentOffset:CGPointZero animated:NO];
		
        return NSNotFound;
    }
    return index - 1; // due to magnifying glass icon
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
	return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	[self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
	id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo name];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            //abort();
        }
    }   
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        StaffPerson *person = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        self.detailViewController.thisPerson = person;
    }
}


#pragma mark segue transistion

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];

		StaffPerson *person;
		if([sender isKindOfClass:[StaffPerson class]]){
			person = (StaffPerson*)sender;

		}else{
			person = [[self fetchedResultsController]objectAtIndexPath:indexPath];
		}
        [[segue destinationViewController] setThisPerson:person];
		[[segue destinationViewController] setSaveObjectContext:[self.fetchedResultsController managedObjectContext]];
		
    }
}


-(void)createNewStaffPerson{
	
	
	
	//[self insertNewObject:[[NSDictionary alloc]init]];
	StaffPerson *newStaffPerson = [NSEntityDescription insertNewObjectForEntityForName:@"StaffPerson" inManagedObjectContext:self.managedObjectContext];
/*	newStaffPerson.lastName = @"";
	newStaffPerson.firstName = @"";
	newStaffPerson.jobTitle = @"";
	newStaffPerson.serviceArea = @"";*/
	newStaffPerson.phone = @"";
	newStaffPerson.email = @"";
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		
		[self.detailViewController setThisPerson:newStaffPerson];
		[self.detailViewController resetView];
		
		//If you're looking for setSaveObjectContext, because this seems like a logical place to put it, its actually in viewDidLoad
		[self.detailViewController setEditing:YES animated:YES];
		//self.detailViewController
	}else{
		[self performSegueWithIdentifier:@"showDetail" sender:newStaffPerson];
	}
}





#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *staffPerson = [NSEntityDescription entityForName:@"StaffPerson" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:staffPerson];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortDescriptors = @[sortDescriptor];
	
	if(!([searchString isEqualToString:@""])){
		NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"lastName CONTAINS '%@' OR firstName CONTAINS '%@'", searchString, searchString]];
		[fetchRequest setPredicate:filterPredicate];
	}
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    // nil for section name key path means "no sections".
	// creating with cache seemed to break the app on search
    //NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"lastInitial" cacheName:@"Master"];
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"lastInitial" cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    
    return _fetchedResultsController;
}






- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    /*NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[object valueForKey:@"firstName"] description];*/
	
	StaffPerson *person = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", person.firstName, person.lastName];
	
}


#pragma mark sendPost Delegate operations


-(void)connection:(NSURLConnection*)connection didFailWithError:(NSError *)error{
	NSLog(@"Error getting data : %@", error);
	UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Could Not Reach Host" message:@"The OSUMC online staff directory could not be reached, please check your settings and ensure you are connected to the network." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
	[networkAlert show];
}

-(void)connection:(NSURLConnection*)connection didReceiveData:(NSData *)data{
	[requestData appendData:data];
	NSLog(@"appending data..");
}

-(void)connectionDidFinishLoading:(NSURLConnection*)connection{
	
	[self processJSONData:requestData];
	
	requestData = [[NSMutableData alloc]init];
	
	
}


-(void)processJSONData:(NSData*)jsonData{
	NSError *jsonError;
	NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
	
	NSDictionary *nodes = [jsonDictionary objectForKey:@"nodes"];
	for (NSDictionary *node in nodes){
		NSLog(@"%@", node);
		//insert or update? based on what?  based on weather this staff person exists and hasn't been edited? should this fetch only be run once? periodically?
		
		NSDictionary *nodeData = [node objectForKey:@"node"];
		[self insertNewObject:nodeData];
		
		
		
		
	}
	//load the first person the first time the json has loaded...

	self.detailViewController.thisPerson = [[self.fetchedResultsController fetchedObjects]objectAtIndex:0];

}



#pragma mark search bar delegate

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
	
	self.searching = YES;
	[self sectionIndexTitlesForTableView:self.tableView];
	[self.tableView reloadData];
	return YES;
	
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
	self.searching = NO;
	[self sectionIndexTitlesForTableView:self.tableView];
	return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
	NSError *err;
	
	searchString = searchText;
		
	//this is the magic line that allows us to quickly reuse our fetchedResultController...very good to know.
	self.fetchedResultsController = nil;
	
	[self.managedObjectContext executeFetchRequest:[self.fetchedResultsController fetchRequest] error:&err];
	[self.tableView reloadData];

}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
	//dissmiss the keyboard.
	self.searching = NO;
	[self sectionIndexTitlesForTableView:self.tableView];
	self.searchBar.text = @"";
	searchString = @"";
	self.fetchedResultsController = nil;
	[self.managedObjectContext executeFetchRequest:[self.fetchedResultsController fetchRequest] error:nil];
	[self.tableView reloadData];

	[searchBar resignFirstResponder];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
	//dissmiss the keyboard
	[searchBar resignFirstResponder];
	
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
	
	[searchBar setShowsCancelButton:YES animated:YES];
	
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
	
	[searchBar setShowsCancelButton:NO animated:YES];
}

#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    return YES;
}
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    return NO;
}



#pragma mark Add myself

-(NSDictionary*)addMyself{
	
	
	
	
	NSDictionary *scottDictionary = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:@"Scott", @"Cheezem", @"614-578-9743", @"Digital Solutions", @"Mobile Developer", @"Contact", @"http://www.cse.ohio-state.edu/~cheezem/avatar.gif", nil] forKeys:[NSArray arrayWithObjects:@"field_firstname", @"field_lastname", @"Phone", @"Service Area", @"field_jobtitle", @"Email", @"field_staffphoto", nil]];
	return scottDictionary;
}


@end
