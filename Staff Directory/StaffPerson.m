//
//  StaffPerson.m
//  Staff Directory
//
//  Created by user on 1/15/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import "StaffPerson.h"


@implementation StaffPerson

@dynamic firstName;
@dynamic lastName;
@dynamic phone;
@dynamic photo;
@dynamic serviceArea;
@dynamic jobTitle;
@dynamic email;
-(NSString*)lastInitial{
	NSString *lastInitial = @"";
	
	[self willAccessValueForKey:@"lastName"];
	if(![[self lastName] isEqualToString:@""]){
		lastInitial = [[self lastName]substringToIndex:1];	
	}
	
	[self didAccessValueForKey:@"lastName"];
	return lastInitial;

}
@end
