//
//  EditTableViewCell.h
//  Staff Directory
//
//  Created by user on 1/19/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditTableViewCell : UITableViewCell{
	UITextField *editTextField;
}

@property (nonatomic, retain) UITextField *editTextField;

@property (nonatomic, retain) id delegate;


@end
