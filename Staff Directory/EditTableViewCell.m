//
//  EditTableViewCell.m
//  Staff Directory
//
//  Created by user on 1/19/13.
//  Copyright (c) 2013 Scott Cheezem. All rights reserved.
//

#import "EditTableViewCell.h"

@implementation EditTableViewCell
@synthesize editTextField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		editTextField.backgroundColor = [UIColor redColor];
		editTextField = [[UITextField alloc]initWithFrame:CGRectMake(self.frame.origin.x+10, self.frame.origin.y+10, self.frame.size.width-40, self.frame.size.height-2)];

		editTextField.font = [UIFont boldSystemFontOfSize:20];
		
		editTextField.delegate = self.delegate;
		editTextField.returnKeyType = UIReturnKeyNext;

		[self.contentView addSubview:editTextField];

		
		
		editTextField.enabled = NO;
		
		//editTextField.hidden = YES;
		//self.textLabel.hidden = YES;
		
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}






@end
